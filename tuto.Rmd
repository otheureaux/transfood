---
title: ""
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Tutoriels {.tabset .unnumbered}

## Flux WFS

```{r, warning=F, message=F}
## Travailler sur la BD TOPO de l'IGN par le service WFS
pacman::p_load(sf, httr, dplyr, ows4r, mapview, purrr)

## Choix du flux
# BATIMENT, ROUTES
URL <- "https://wxs.ign.fr/essentiels/geoportail/wfs?VERSION=2.0.0"
# DEPARTEMENT
url_bd <- "https://wxs.ign.fr/topographie/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"
# IRIS
url_bd <- "https://wxs.ign.fr/cartovecto/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"

# Interroger le contenu du lien 
ign_client <- ows4R::WFSClient$new(url_bd, serviceVersion = "2.0.0") 
ign_client$getFeatureTypes(pretty = TRUE)
# options(max.print = 1000)
# a <- bd_vrbg$getFeatureTypes(pretty = TRUE)
# b <- a%>%filter(grepl(pattern = "iris", x = name))

# Interroger le contenu d'un objet : Ici je cherche les variables de l'élément commune
ign_client$
  getCapabilities()$
  findFeatureTypeByName("BDCARTO_BDD_WLD_WGS84G:commune")$
  getDescription() %>%
  purrr::map_chr(function(x){x$getName()})

# isoler des données de la BDTOPO avec une bounding box
parse_url <- httr::parse_url(url_bd)
parse_url$query <- list(service = "WFS",
                        #version = "2.0.0", # optional
                        request = "GetFeature",
                        typename = "BDCARTO_BDD_WLD_WGS84G:commune", # type disponible ici : ign_client$getFeatureTypes(pretty = TRUE)
                        cql_filter = c("insee_com='34172'")) # J'utilise insee_com repéré à l'étape précédente pour charger l'emprise de Montpellier
                        # cql_filter = paste("code_insee='77316'", "code_insee='77186'", "code_insee='77463'", "code_insee='77419'", "code_insee='77014'", "code_insee='77079'", sep = " OR "))
                        # bbox = "48.88417,2.34466,48.90956,2.38308") # epsg:2154
request <- st_read(build_url(parse_url))
```


```{r, warning=F, message=F, out.width= "100%"}
request_cast <- st_make_valid(st_cast(request, "GEOMETRYCOLLECTION"))
mapview(request_cast)
```

## OSMEXTRACT

### Chargement des packages

```{r, eval = F}
pacman::p_load(osmdata, sf, dplyr, mapview, httr)

url <- "https://wxs.ign.fr/topographie/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"
parse_url <- parse_url(url)
parse_url$query <-list(service = "wfs", version = "2.0.0", crs = 4326,
                       request = "GetFeature", typename = "BDTOPO_V3:departement")
             
departements <- st_read(build_url(parse_url))
departements_metro <- departements %>% 
  filter(!grepl("^(971|972|973|974|976)", code_insee)) # exclusion des caractères commençant par 971|972|973|974|976. Le ^ permet cela. 
france_emprise <- st_union(departements_metro)
mapview(france_emprise)


# Initialisation du fichier source
commerces_osm_all <- tibble()
```


```{r, eval = F}
# Chargement des packages
pacman::p_load(osmdata, sf, dplyr, mapview, httr, osmextract)
getwd()



commerces_osm_all <- st_read("processed_data/organic_osmextract_v18.gpkg") %>% rename(geometry = geom)

region_osm <- c("Midi-Pyrenees", "Provence Alpes-Cote-d'Azur", "Champagne Ardenne","Bourgogne", 
         "Franche Comte", "Auvergne", "Alsace", "Lorraine", 
         "Picardie", "Bretagne", "Rhone-Alpes", "Languedoc-Roussillon", 
         "Ile-de-France", "Centre", "Nord-Pas-de-Calais", "Corse",
         "Basse-Normandie", "Haute-Normandie", "Pays de la Loire", "Aquitaine",
         "Limousin", "Poitou-Charentes")

for (i in 19:22) {


# couche de points
npdc_data_points = osmextract::oe_get(region_osm[i],
                                        layer = "points",
                                        extra_tags = "organic", 
                                        query = "SELECT * FROM points WHERE organic IN ('yes', 'only')",
                                        quiet = FALSE)

# couche de polygons
npdc_data_polygons = osmextract::oe_get(region_osm[i],
                                          layer = "polygons",
                                          extra_tags = "organic", 
                                          query = "SELECT * FROM multipolygons WHERE organic IN ('yes', 'only')",
                                          quiet = FALSE)

data_points <- npdc_data_points
mapview(data_points)
data_polygons <- npdc_data_polygons
mapview(data_polygons)

# region <- departements %>% filter(code_insee_de_la_region == '75')
# mapview(data_points) + mapview(data_polygons) + mapview(region)

# shop_67_points <- st_intersection(france_emprise,alsace_data_points)
# dim(shop_67_points)
# shop_67_polygons <- st_intersection(france_emprise,(st_make_valid(alsace_data_polygons)))
# dim(shop_67_polygons)
# mapview(shop_67_points) + mapview(shop_67_polygons)

shop_67_points <- st_make_valid(data_points)
shop_67_polygons <- st_make_valid(data_polygons)

# deduplicate shops
if(nrow(shop_67_polygons) > 0) {
  osm_points_in_polygons = shop_67_points[shop_67_polygons, ]
  osm_points_not_in_polygons = shop_67_points[
    !shop_67_points$osm_id %in% osm_points_in_polygons$osm_id,
  ] 
  
  # convert polygons to points and join together
  osm_polygons_centroids = sf::st_centroid(shop_67_polygons)
  setdiff(names(shop_67_points), names(osm_polygons_centroids))
  names_in_both = intersect(names(shop_67_points), names(osm_polygons_centroids))
  osm_points = rbind(osm_points_not_in_polygons[names_in_both], osm_polygons_centroids[names_in_both])
} 

dim(osm_points)
mapview(osm_points)

names(osm_points)

osm_points_light <- osm_points %>% 
  select(osm_id,name,organic,geometry)
# commerces_osm_all <- st_read("processed_data/organic_osmextract_v1.gpkg") %>% rename(geometry = geom)
commerces_osm_all <- rbind(data.frame(osm_points_light), commerces_osm_all)

rm(data_points)
rm(data_polygons)
rm(osm_points)
rm(shop_67_points)
rm(shop_67_polygons)
rm(osm_points_not_in_polygons)
rm(osm_points_in_polygons)
rm(osm_polygons_centroids)

# getwd()
# list.files()
commerces_osm_all_sf <- st_as_sf(commerces_osm_all)
st_write(commerces_osm_all_sf, paste0("processed_data/organic_osmextract_v",i,".gpkg"))

}
```


```{r, eval = F}
# Comptage des commerces avec group_by
pacman::p_load(sf, dplyr)
commerces_osm_all <- st_read("processed_data/commerces_osmextract_v19.gpkg") %>% rename(geometry = geom)
names(commerces_osm_all)
data_com <- commerces_osm_all %>% 
  group_by(shop) %>%
  summarise(count = n()) %>%
  arrange(desc(count))

data_com

st_write(commerces_osm_all_sf, "processed_data/frozen_food_osmextract_v11.gpkg")
```



```{r, eval = F}
pacman::p_load(osmextract, mapview, sf)

metropole_lyon <- sf::st_read('C:/Users/otheureaux/Dropbox (6t)/6t/Affaires France/EN COURS/Grand Lyon - Etudes stratégiques de mobilité - M6-M7/Analyses/Donnees cartographiques/Couches_SIG/metropole_GL.gpkg')


# Requête simple avec bbox textuelle
lyon_data_ways <- osmextract::oe_get('Lyon',
                                     layer = "lines", # points, multilinestring, multipolygons
                                     query = "SELECT * FROM lines WHERE busway = 'lane' ",
                                     quiet = FALSE)

# Requête simple avec bbox textuelle
lyon_data_ways <- osmextract::oe_get(metropole_lyon,
                                     layer = "lines",
                                     query = "SELECT * FROM lines WHERE busway = 'lane' ",
                                     quiet = FALSE)

# Requête simple avec plusieurs values
lyon_data_ways <- osmextract::oe_get('Lyon', 
  provider  = 'geofabrik',
  layer = "multipolygons",
  extra_tags = c("busway"),
  query = "SELECT * FROM multipolygons WHERE busway IN ('lane', 'right=lane', 'both=lane', 'left=lane', 'opposite_lane' )"
)

# Requête simple avec plusieurs keys et plusieurs values
motorway_links_for_buses_bbbike <- osmextract::oe_get(
  'Paris', 
  provider = 'geofabrik',
  layer = "lines",
  extra_tags = c("highway", "access"),
  query = "SELECT * FROM lines WHERE highway IS NOT NULL AND access = 'bus'"
)


mapview(motorway_links_for_buses_geofabrik)
sf::st_write(motorway_links_for_buses_geofabrik, 'C:/Users/otheureaux/Downloads/motorway_links_for_buses_geofabrik6.shp')
```



## QGISPROCESS

### Rechercher un algorythm

```{r, eval = F}

# Chargement de la library
pacman::p_load(qgisprocess)

# Permet de créer un vecteur avec tous les algorithmes
qgis_algo <- qgis_algorithms()

# Rechercher un traitement spécifique
grep("intersect", qgis_algo$algorithm, value = T)
grep("grass", qgis_algo$algorithm, value = T)
qgis_algo$provider_title

qgis_configure()

# Montrer l'aide
qgis_show_help("native:lineintersections")
```


## Géocodage

```{r, eval = F}

La première partie de la mission consiste à géocoder une grand volume d'adresses. 

Le géocodage consiste à assigner des coordonnées latitude et longitude à une adresse en la comparant à des adresses de références. Il s'agit donc de normaliser l’adresse puis de faire une corrélation avec la base de référence. 
Informations sur le géocodage (makina-corpus.com) [lien](https://makina-corpus.com/sig-webmapping/geocodeur-la-theorie)

Avant de procéder au géocodage en utilisant l'outil de géocodage proposer par le site
[https://adresse.data.gouv.fr/csv](https://adresse.data.gouv.fr/csv), nous faisons appel à différentes fonctions de manipulation de données (*arrange*, *lag*, *subset*, ...) qui permettent de supprimer les doublons.

Une dernière étape consiste en une "relecture" manuelle de chaque ligne où le *result score* obtenu lors du géocodage est inférieur à 0,5.

### Techniques du géocodage

Sites et API
* Geocoder sur le site du gouvernement
* Via l'API Google Maps
* Via Openstreet Map

### Qualité du géocodage

Biblio sur le "score de qualité"

Le géocodeur ne sait pas ce qui est cherché, et ne peut donc pas déterminer de score par rapport à un résultat absolu.
Pour déterminer le score (qui sert seulement à ordonner les résultats, et n'est retourné que pour information), il compare *grosso modo* la chaîne en entrée avec les résultats possibles. Donc les score peuvent varier selon la qualité des données en entrée et leur conformité aux règles habituelles de nommage des adresses (numéro + rue + code postal + ville, en très simplifié).
Bref, c'est une indication, qu'il faut ensuite ou bien ignorer ou bien interpréter selon le jeu de données utilisé.
Source : [lien](https://github.com/addok/addok/issues/153)

Chaque résultat contient également une ou plusieurs notions de qualité pour ordonner les résultats entre eux. Le géocodeur calcule un score, mais ce score n’est pas toujours disponible en sortie. Il n’est souvent qu’une mesure interne et spécifique à une recherche.
Source : [lien](https://makina-corpus.com/sig-webmapping/les-logiciels-et-api-pour-geocoder)

### Manipulation du  data frame 

* supprimer les doublons

### Data.gouv.fr

* lancer le géocodage sur https://adresse.data.gouv.fr/csv